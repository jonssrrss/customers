$(function(){

    $('input[name=phone]').mask("8(999) 999-9999");

    // Обработчик нажатий на кнопку
    $('button').on('click', function() {

        var name = $(this)[0].name;
        
        switch (name) {
            case 'new-contact':
                var name = $('form.add-number input[name=name]').val(),
                    phone = $('form.add-number input[name=phone]').val(),
                    email = $('form.add-number input[name=email]').val(),
                    source_id = $('form.add-number input[name=source_id]').val();

                cust_data.add(
                    {
                        'source_id': source_id,
                        'items': [
                            {
                                'name': name,
                                'phone': phone,
                                'email': email,
                            }
                        ]
                    }
                );
                break;
            case 'search':
                var phone = $('form.search-number input[name=phone]').val();
                cust_data.search(phone);
            break;
        }

        return false;
        
    });

});