/**
 * Работа с клиентскими данными
 */
var cust_data = {

    add: function(data) {
        
        $.post(
            '/contacts',
            data,
            function(response) {
                resp = JSON.parse(response);
                if (resp.res != undefined) {
                    alert('Добавлено. Количество записей в бд - ' + resp.res);
                    $('.add-number input').val('');
                }
                if (resp.err != undefined) {
                    alert('Ошибка. ' + resp.err);
                }
            }
        );
    },

    search: function(phone) {
        $.post(
            '/contacts?phone=' + phone,
            {},
            function(response) {
                resp = JSON.parse(response)['res'];
                var table = '';

                if (resp.length > 0) {
                    table += '<table>';
                    table += '<th>Имя</th>';
                    table += '<th>Почта</th>';
                    table += '<th>Телефон</th>';
                    for (item in resp) {
                        console.log(resp[item]);
                        table += '<tr>';
                        table += '<td>' + resp[item]['name'] + '</td>';
                        table += '<td>' + resp[item]['email'] + '</td>';
                        table += '<td>' + resp[item]['phone'] + '</td>';
                        table += '</tr>';
                    }
                    table += '</table>';
                }

                $('.contact-list').html(table);
            }
        );
    }

}