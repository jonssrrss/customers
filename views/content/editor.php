<form class="add-number">

    <h3>Добавить новый контакт</h3>

    <label for="source_id">Source_id</label>
    <input type="text" name="source_id">

    <label for="name">Имя</label>
    <input type="text" name="name">

    <label for="phone">Телефон</label>
    <input type="text" name="phone">

    <label for="email">Почта</label>
    <input type="text" name="email">

    <button name="new-contact">Добавить новый контакт</button>

</form>


<form class="search-number">

    <h3>Поиск контактов</h3>

    <label for="phone">Телефон</label>
    <input type="text" name="phone">

    <button name="search">Найти</button>

    <div class="contact-list"></div>

</form>