<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customer data</title>
    <link rel="stylesheet" href="/assets/css/style.css?v=1">
</head>
<body>

    <div class="wrapper">
        <header>
            <? if ($user::isAuth()) {
                echo '<a href="/logout">Выход</a>';
            } ?>
        </header>
        <? require_once($contentFile); ?>
    </div>

    <? if ($user::isAuth()) { ?>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="/assets/js/jquery.maskedinput.min.js"></script>
        <script src="/assets/js/customer_data.js?v=1"></script>
        <script src="/assets/js/script.js?v=1"></script>
    <? } ?>

</body>
</html>