<?
	class IndexController
	{

		public static function action()
		{
			global $user;

			if (isset($_POST['login'])) {
				$username = $_POST['username'];
				$password = $_POST['password'];
	
				$user::login($username, $password);
			}

			if (!$user::isAuth())
				$contentFile = ROOT.'/views/content/login.php';
			else
				$contentFile = ROOT.'/views/content/editor.php';

			require_once ROOT.'/views/template/index.php';
		}
		
		public static function actionLogout()
		{
			session_destroy();
			header('Location: /');
		}

	}