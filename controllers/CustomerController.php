<?
	class CustomerController
	{

		public static function action()
		{
			global $user;

            try {
                if (!$user::isAuth())
                    throw new Exception('Ошибка доступа');

                if ( !isset($_GET['phone']) && (!isset($_POST['source_id']) || !isset($_POST['items'])) )
                    throw new Exception('Недостаточно параметров');

                if (isset($_POST['source_id']))
                    foreach ($_POST['items'] as $key => $item) {
                        if (!isset($item['name']) || !isset($item['phone']) || !isset($item['email']))
                            throw new Exception('Недостаточно параметров');
                    }

                require_once ROOT.'/models/Customer.php';

                if (isset($_GET['phone'])) {
                    $phone = $_GET['phone'];
                    $res = Customer::search($phone);
                } else {
                    foreach ($_POST['items'] as $key => $item) {
                        $result = Customer::add($_POST['source_id'], $item['name'], $item['phone'], $item['email']);

                        if (isset($result['err']))
                            throw new Exception($result['err']);
                    }
                    $res = Customer::countContacts();
                }

                echo json_encode(['res' => $res]);
            } catch (Exception $e) {
                echo json_encode(['err' => $e->getMessage()]);
                return;
            }
        }

    }