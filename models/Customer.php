<?
    /**
     * Работа с клиентскими данными
     */
    class Customer
    {

        /**
         * Поиск данных по номеру
         */
        public static function search($phone)
        {
            global $db;

            $phone = preg_replace("/[^0-9]/", '', $phone);

            if ((int)$phone[0] != 9)
                $phone = substr($phone, 1);

            $qr = 'SELECT * FROM contacts WHERE phone = \'' . $phone . '\'';
            return $db->query($qr);
        }


        /**
         * Добавление данных в базу
         */
        public static function add($source_id, $name, $phone, $email)
        {
            try {
                global $db;
                $phone = preg_replace("/[^0-9]/", '', $phone);

                if ($source_id == '')
                    throw new Exception('Необходимо указать source_id');

                if (!preg_match('/^\+?\d+$/', $source_id))
                    throw new Exception('source_id должен быть целым числом');

                if (strlen($phone) == 0)
                    throw new Exception('Необходимо указать номер телефона');

                if (!preg_match('/^[a-zA-Zа-яА-Я]+$/ui', $name))
                    throw new Exception('Напишите настоящее имя');

                if (
                    !filter_var($email, FILTER_VALIDATE_EMAIL) ||
                    strlen($phone) != 11
                )
                    throw new Exception('Проверьте почту и телефон');

                if ((int)$phone[0] != 9)
                    $phone = substr($phone, 1);

                $qr = "
                    SELECT
                        *
                    FROM
                        `contacts`
                    WHERE
                        phone = '" . $phone . "' AND
                        source_id = '" . $source_id . "' AND
                        date > UNIX_TIMESTAMP(CONCAT(date_format(curdate() - INTERVAL 1 DAY, '%Y-%m-%d'), ' 00:00:00'))
                ";

                if (count($db->query($qr)) > 0)
                    throw new Exception('Сегодня уже был такой номер для данного source_id');

                $qr = "
                    INSERT INTO `contacts`(
                        `id`,
                        `source_id`,
                        `name`,
                        `phone`,
                        `email`,
                        `date`
                    )
                    VALUES(
                        NULL,
                        '" . $source_id . "',
                        '" . $name . "',
                        '" . $phone . "',
                        '" . $email . "',
                        '" . time() . "'
                    );
                ";

                $db->query($qr);
                return true;
            } catch (Exception $e) {
                return ['err' => $e->getMessage()];
            }
        }

        /**
         * Посчитать количества записей клиентских данных
         */
        public static function countContacts()
        {
            global $db;

            $qr = 'SELECT count(phone) AS count FROM contacts';
            return $db->query($qr)[0]['count'];
        }

    }